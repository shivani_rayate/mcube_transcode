const mongoose = require('mongoose');
const MongoClient = require('mongodb').MongoClient;

const mongoUrl = "mongodb://localhost:27017/";

const config = require('../config/config.json');
const mongodbConfig = config.mongodb["development"];


exports.getMongoDbs = () => {

    return new Promise((resolve, reject) => {

        const client = new MongoClient(mongoUrl, { useUnifiedTopology: true });

        // Connect
        client
            .connect()
            .then(client =>
                client
                    .db()
                    .admin()
                    .listDatabases() // Returns a promise that will resolve to the list of databases
            )
            .then(dbs => {
                //console.log("Mongo databases", dbs);
                resolve(dbs.databases)
            })
            .finally(() => client.close()); // Closing after getting the data
    })
}


exports.connectMongoDb = (mongodbName) => {

    return new Promise((resolve, reject) => {

        const options = {
            socketTimeoutMS: 30000,
            keepAlive: true,
            poolSize: 50,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            autoIndex: false
        };

        //const connectionString = `${mongodbConfig.url}${mongodbConfig.username}:${mongodbConfig.password}@${mongodbConfig.host}:${mongodbConfig.port}:${mongodbName}`;
        const connectionString = `mongodb://localhost:27017/${mongodbName}`;
        //const connectionString = `${process.env.connectionString}/${mongodbName}`;


        console.log(`\n connectionString: ${connectionString}`);

        const db = mongoose.createConnection(connectionString, options);

        db.on("error", console.error.bind(console, "\n MongoDB Connection Error>> : "));

        db.on('connected', () => {

            console.log('\n connected to mongodb > ' + mongodbName);

            // here we assign connection object to the global js object
            global.clientConnection = db;

            resolve(db);
        });
    })
};