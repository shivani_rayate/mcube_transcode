var express = require('express');
var router = express.Router();
const AWS = require('aws-sdk');

const Profiles = require("../controllers/profiles");
const profilesObj = new Profiles.ProfilesClass();

const awsConfig = require('../../config/aws_config')['development'];
const tenantConfig = require('../../config/tenantConfig');


AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

const mediaConvertUtil = require('../app/utilities/video/media_convert');



/* POST : SAVE Profiles DETAILS. */
router.post('/', (req, res, next) => {
    console.log("Profiles POST called")

    const mediaParams = req.body.profiles ? req.body.profiles : null;

    mediaConvertUtil.commonJSON().then((unformattedMediaJSON) => {
        mediaConvertUtil.forMatMediaJSONByPublisher(unformattedMediaJSON, mediaParams).then((mediaJSON) => {

            const params = {
                profile_name: req.body.profile_name ? req.body.profile_name : null,
                profiles: req.body.profiles ? req.body.profiles : null,
                mediaJSON: mediaJSON ? mediaJSON : null
            }


            profilesObj.save(params).then((_response) => {
                if (_response) {
                    console.log('Profiles data saved');
                    res.json({
                        status: 200,
                        message: 'Profiles Saved successfuly',
                        data: _response,
                    })

                } else {
                    console.log('Profiles data save failed');
                    res.json({
                        status: 401,
                        message: 'Profiles Save failed',
                        data: null,
                    })
                }
            })

        })
    })

});


// Get All Profiles
router.get('/getAllProfiles', (req, res, next) => {

    console.log('\n getAllProfiles called in mcube_transcode');

    let tenantId = res.locals.tenantId;

    const params = {
        mongoDbName: tenantConfig[tenantId].config.mongodb
    };

    profilesObj.findall(params).then((profiles) => {
        res.json({
            status: 200,
            message: 'Profiles fetched successfully',
            data: profiles,
        });
    });

});



// #############################################################################

//delete profile
router.delete('/deleteProfile/:_id', function (req, res, next) {

    var params = {
        _id: req.params._id
    }
    profilesObj.deleteProfile(params).then((response) => {

        if (response) {
            res.json({
                status: 200,
                message: "Profile deleted successfully",
                data: response
            })

        } else {
            res.json({
                status: 400,
                message: "Profile delete failed",
                data: null
            })
        }
    });

});


//##########################################################################


// preload profile data
router.post('/preload_profiledata', (req, res, next) => {

    const params = {
        profile_name: req.body.profile_name

    };

    profilesObj.findDataByid(params).then((data) => {
        res.json({
            status: 200,
            message: 'Metadata Loaded successfully',
            data: data,
        });
    });

});

module.exports = router;