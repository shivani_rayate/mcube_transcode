const express = require('express');
const router = express.Router();
const videoPartial = require('../app/utilities/video/index');

const Video = require('../controllers/video');
const videoObj = new Video.VideoClass();

const Profiles = require("../controllers/profiles");
const profilesObj = new Profiles.ProfilesClass();

let video_utility = require('../app/utilities/video');

const multer = require('multer');
const AWS = require('aws-sdk');

const multerS3 = require('multer-s3');
const s3 = new AWS.S3();

const uuidv1 = require('uuid/v1');
const awsConfig = require('../../config/aws_config')['development'];
const tenantConfig = require('../../config/tenantConfig');

const checkAuth = require('../middleware/check-auth');
const listS3 = require('../app/utilities/listS3');



router.get('/list', (req, res, next) => {
    console.log('video /list called in mcube_transcode');

    let tenantId = res.locals.tenantId;

    const params = {
        mongoDbName: tenantConfig[tenantId].config.mongodb
    };

    videoObj.findall(params).then((videos) => {
        res.json({
            status: 200,
            message: 'Videos fetched successfully',
            data: videos,
        });
    }).catch((err) => {
        res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
        });
    });
})


// getlink preview asset overView
router.post('/getlink-preview', (req, res, next) => {
    console.log("getlink preview asset OverView POST CALLED in API")

    let tenantId = res.locals.tenantId;

    let params = {
        key: req.body.key,
        bucket: tenantConfig[tenantId].config.config.s3BucketList.outputBucket
    };

    console.log("getLink review asset overview in route >> >> " + JSON.stringify(params))
    listS3.getlink_preview_asset(params).then((url) => {
        if (res) {
            res.json({
                status: 200,
                message: "getlink Preview asset overView successfully",
                data: url
            })
        } else {
            res.json({
                status: 400,
                message: "getlink failed",
                data: null
            })
        }
    })

});


router.get('/todays_uploaded', (req, res, next) => {

    let tenantId = res.locals.tenantId;

    const params = {
        mongoDbName: tenantConfig[tenantId].config.mongodb
    };

    videoObj.countTodaysVideo(params).then((count) => {
        res.json({
            status: 200,
            message: 'Todays Videos Counted successfully',
            data: count,
        });
    });

});


// Transcode-asset
router.post('/transcode_asset', function (req, res, next) {

    console.log("transcode_asset called in mcube_transcode API > ")

    let tenantId = res.locals.tenantId;
    let content_id = req.body.content_id;
    let profiles = req.body.profiles;
    let content_path = req.body.content_path;

    let params = {
        tenantId: tenantId,
        content_path: content_path,
        profiles: profiles,
    };

    video_utility.transcode_asset(params).then((response) => {

        if (response) {
            res.json({
                status: 200,
                message: "Asset Transcode is in Progress",
                data: null
            })

            profiles.forEach((element, i) => {

                var tableName;
                switch (profiles[i]) {
                    case 'basic':
                        transcode_type = 'basic';
                        tableName = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.basicProfileTbl;
                        break;
                    case 'standard':
                        transcode_type = 'standard';
                        tableName = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.standardProfileTbl;
                        break;
                    case 'premium':
                        transcode_type = 'premium';
                        tableName = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.premiumProfileTbl;
                }

                var _params = {
                    tableName: tableName,
                    contentId: content_id,
                    transcode_type: transcode_type,
                    tenantId: tenantId,
                };

                //console.log("\n \n \n PARAMS AT >> " + JSON.stringify(_params))

                video_utility.getDynamodbStatus(_params);

            });

        } else {
            res.json({
                status: 400,
                message: "Asset Transcode failed",
                data: null
            })
        }
    }).catch((err) => {
        console.log(`error IN  asset transcode  : ${err}`);
        res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
        });
    });
})

// Transcode-custom-asset
router.post('/transcode_custom_asset', function (req, res, next) {

    console.log("\n /transcode_custom_asset called in API ")

    let tenantId = res.locals.tenantId;
    var contentId = req.body.content_id;
    var profiles = req.body.profiles;
    var content_path = req.body.content_path;
    let CloudFront = req.body.CloudFront;

    var input_bucket = tenantConfig[tenantId].config.config.s3BucketList.inputBucket;
    var output_bucket = tenantConfig[tenantId].config.config.s3BucketList.outputBucket;
    var clientId = tenantConfig[tenantId].config.clientId;

    profiles.forEach((element, i) => {

        let profile_params = {
            profile: profiles[i],
            mongoDbName: tenantConfig[tenantId].config.mongodb
        }

        profilesObj.findMediaJson(profile_params).then((profileJson) => {

            console.log("\n \n rofile name Json >> >>> " + JSON.stringify(profileJson))

            var profile = profileJson[0].mediaJSON;
            profile = JSON.parse(JSON.stringify(profile));
            var custP = [];
            //assetID
            profile.UserMetadata.assetid = contentId;
            profile.UserMetadata.profile_id = `skn-c`;
            profile.UserMetadata.clientId = clientId;
            var custom_resolution = profileJson[0].profiles[0].height;
            var custom_Profile = profileJson[0].profiles;
        
            console.log("\n \n height>> >>> " + JSON.stringify(custom_Profile))
          
            custom_Profile.forEach(function (element, i){
                var height = element.height ? element.height : "";
               // console.log("\n \n height>> >>> " + JSON.stringify(height))
               custP.push(height)
            })

            // input filename
            profile.Settings.Inputs[0].FileInput = `s3://${input_bucket}/transcode/input-data/${contentId}/${contentId}${awsConfig.video.extension}`;
            // Destination file
            profile.Settings.OutputGroups[0].OutputGroupSettings.HlsGroupSettings.Destination = `s3://${output_bucket}/transcode/processed-data/${contentId}/${profileJson[0].profile_name}/index`;

            //console.log("PROFILE HERE >> "+ JSON.stringify(profile))

            videoPartial.mediaCreateJob(profile).then((mediaObj) => {
                console.log("\n \n mediaObj>> >>> " + JSON.stringify(mediaObj))
                // ************************** //
                delete AWS.config['endpoint']; // ISSUE IS MC
                // ************************** //

                if (mediaObj.JobId) {

                    let _params = {
                        contentId: contentId,
                        jobId: mediaObj.JobId,
                        playback_url: `https://${output_bucket}.s3.ap-south-1.amazonaws.com/transcode/processed-data/${contentId}/${profileJson[0].profile_name}/index.m3u8`,
                        tableName: tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.customProfileTbl,
                        custom_resolution:custP
                    }

                    video_utility.updateDynamodb(_params).then((err, data) => {

                        if (err) {
                            reject(err);
                        } else {
                            let videoData = {
                                tenantId: tenantId,
                                contentId: contentId,
                                media_job_id: mediaObj.JobId,
                                tableName: tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.customProfileTbl,
                                CloudFront_URL:CloudFront
                            };
                            // processMediaConvertV2
                            videoPartial.processMediaConvertV2(videoData);

                            res.json({
                                status: 200,
                                message: "Asset Transcode is in Progress",
                                data: null
                            })

                        }
                    })

                } else {
                    console.log('ERROR IN MEDIA CONVERT!!!!!!!');
                }
            });

        })

    })

})





router.post('/get_asset_status', (req, res, next) => {

    let tenantId = res.locals.tenantId;

    const params = {
        content_id: req.body.content_id,
        mongoDbName: tenantConfig[tenantId].config.mongodb
    };

    console.log('***GET ASSET STATUS PARAMS ***' + JSON.stringify(params));
    videoObj.findStatusByid(params).then((videos) => {
        res.json({
            status: 200,
            message: 'Status fetched successfully',
            data: videos,
        });
    });
});

//delete video
router.delete('/deletevideo/:content_id', checkAuth, function (req, res, next) {

    let tenantId = res.locals.tenantId;

    var params = {
        content_id: req.params.content_id,
        mongoDbName: tenantConfig[tenantId].config.mongodb
    }

    videoObj.deletevideo(params).then((response) => {

        if (response) {
            res.json({
                status: 200,
                message: "Video delete successfully",
                data: response
            })

        } else {
            res.json({
                status: 400,
                message: "Video Not delete",
                data: null
            })
        }
    });

});

// save video metadata
router.post('/metadatasave', (req, res, next) => {

    let tenantId = res.locals.tenantId;

    // console.log("checking for array here >>> "+ JSON.stringify(req.body))
    const params = req.body;
    params.mongoDbName = tenantConfig[tenantId].config.mongodb;
    params['created_by'] = 'Admin'

    videoObj.findOneAndUpdate(params).then((response) => {
        if (response) {

            console.log('meta data saved');
            res.json({
                status: 200,
                message: 'Video Saved successfuly',
                data: response,
            })

        } else {
            res.json({
                status: 400,
                message: 'Video Save failed',
                data: response,
            })
        }
    })
})

// preload metadata Asset
router.post('/preload_metadata', (req, res, next) => {

    console.log('\n preload_metadata called in API');

    let tenantId = res.locals.tenantId;

    const params = {
        content_id: req.body.content_id,
        mongoDbName: tenantConfig[tenantId].config.mongodb
    };

    videoObj.findDataByid(params).then((data) => {
        res.json({
            status: 200,
            message: 'Metadata Loaded successfully',
            data: data,
        });
    });

});


module.exports = router;
