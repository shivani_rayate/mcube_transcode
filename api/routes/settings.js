var express = require('express');
var router = express.Router();
const AWS = require('aws-sdk');

const Config = require("../controllers/setting");
const settingObj = new Config.SettingClass();

const awsConfig = require('../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

// const mediaConvertUtil = require('../app/utilities/video/media_convert');


/* POST : SAVE Configuration DETAILS. */
router.post('/', (req, res, next) => {
    console.log("configuration POST called")
    let params = {
        name: req.body.name,
        config: req.body.config ? req.body.config : null,
    }
    console.log(" configuration >>>" + JSON.stringify(params))

    settingObj.save(params).then((_response) => {
        if (_response) {
            console.log('Configuration data saved');
            res.json({
                status: 200,
                message: 'Saved successfuly',
                data: _response,
            })

        } else {
            console.log('Configuration data save failed');
            res.json({
                status: 401,
                message: 'Configuration Save failed',
                data: null,
            })
        }
    })
})

// update Configuration
router.post('/updateConfig', (req, res, next) => {
    console.log("updateConfig called")
    var params = {
        name: req.body.name
    }

    console.log(" update config data >>>" + JSON.stringify(params))

    settingObj.findOneAndUpdate(params).then((_response) => {
        if (_response) {
            console.log('update data saved');
            res.json({
                status: 200,
                message: 'Saved successfuly',
                data: _response,
            })

        } else {
            console.log('update data save failed');
            res.json({
                status: 401,
                message: 'update Save failed',
                data: null,
            })
        }
    })
})

module.exports = router;