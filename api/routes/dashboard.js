var express = require('express');
var router = express.Router();

const Video = require('../controllers/dashboard');
const dashboardObj = new Video.DashboardClass();

let video_utility = require('../app/utilities/video');
// get todaysuploaded_list 
router.post('/todays_uploaded', (req, res, next) =>{
  const params = {

    };
   dashboardObj.countTodaysVideo(params).then((data) =>{
        res.json({
            status: 200,
            message: 'Todays Videos Counted successfully',
            data: data,
        });
    });
});

// get processing_list 
router.post('/proccessing_list', (req, res, next) => {
    const params = {

        profiles: { basic: { status: "PROCESSING" } }

  };
    console.log('***GET PROCESSING LIST PARAMS ***' + JSON.stringify(params));
    dashboardObj.findStatusByid(params).then((data) => {
        console.log('***GET PROCESSING LIST PARAMS ***' + JSON.stringify(data));
         res.json({
            status: 200,
            message: 'List fetched successfully',
            data: data,
        });
    });
});

// get transcoded_list 
router.post('/transcoded_list', (req, res, next) => {

    const params = {


    };
    console.log('***GET TRANSCODED LIST PARAMS ***' + JSON.stringify(params));
    dashboardObj.findlistBystatus(params).then((data) => {
          res.json({
            status: 200,
            message: 'List of transcoded videos fetched successfully',
            data: data,
        });
    });
});
module.exports = router;