// Load the SDK for JavaScript
var AWS = require('aws-sdk');

const awsConfig = require('../../../config/aws_config')['development'];
const tenantConfig = require('../../../config/tenantConfig');

const video_utility = require('../../app/utilities/video');

const Video = require('../../controllers/video');
const videoObj = new Video.VideoClass();

const videoPartial = require('../../app/utilities/video/index');


AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});


// To read getDynamodbStatus
exports.getDynamodbStatus = (reqParams) => {

    console.log("getDynamodbStatus called with params > " + JSON.stringify(reqParams))

    let tenantId = reqParams.tenantId ? reqParams.tenantId : null;

    return new Promise((resolve, reject) => {

        var params = {
            TableName: reqParams.tableName,
            Key: {
                'assetid': reqParams.contentId,
            }
        };

        //console.log("AWS CONFIG ## > ", JSON.stringify(AWS.config))

        // ************************** //
        // FIX FOR ERROR IN READING ITEM FROM DYNAMODB
        // Unable to read item. Error ForbiddenException: Forbidden
        delete AWS.config['endpoint']; // ISSUE IS MC
        // ************************** //

        var docClient = new AWS.DynamoDB.DocumentClient();

        const statusJobPromise = docClient.get(params).promise();

        statusJobPromise.then(
            (data) => {

                console.log("GetItem from DynamoDB succeeded : > " + JSON.stringify(data));

                if (data.Item && (data.Item.status === 'COMPLETE' || data.Item.status === 'ERROR')) {

                    let _params;

                    switch (reqParams.transcode_type) {
                        case 'preview':
                            _params = {
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                media_job_status: data.Item.status ? data.Item.status : null,
                                media_job_id: data.Item.job_id_mc ? data.Item.job_id_mc : null,
                                preview_url: data.Item.preview_url ? data.Item.preview_url : null,
                                video_path: data.Item.video_path ? data.Item.video_path : null,
                            }

                            // get thumbnails and update
                            let getThumbnailParams = {
                                media_job_status: data.Item.status ? data.Item.status : null,
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                            }

                            videoPartial.listS3AndGetThumbnail(getThumbnailParams).then((thumbnails) => {

                                const updateParamsThumbnail = {
                                    content_id: reqParams.contentId,
                                    thumbnail: (thumbnails || []),
                                    thumbnail_pic: (data.Item.status === 'COMPLETE' && thumbnails.length < 3 ? thumbnails[0] : (thumbnails[2] || ''))
                                };

                                // mongDb name
                                updateParamsThumbnail.mongoDbName = tenantConfig[tenantId].config.mongodb;

                                videoObj.findOneAndUpdate(updateParamsThumbnail).then((rs) => {
                                    console.log('VIDEO THUMBNAILS UPDATE COMPLETE!!!!');
                                });

                            });

                            break;
                        case 'basic':
                            _params = {
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                basic_transcode_status: data.Item.status ? data.Item.status : null,
                                basic_transcode_url: data.Item.playback_url ? data.Item.playback_url : null,
                            }
                            break;
                        case 'standard':
                            _params = {
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                standard_transcode_status: data.Item.status ? data.Item.status : null,
                                standard_transcode_url: data.Item.playback_url ? data.Item.playback_url : null,
                            }
                            break;
                        case 'premium':
                            _params = {
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                premium_transcode_status: data.Item.status ? data.Item.status : null,
                                premium_transcode_url: data.Item.playback_url ? data.Item.playback_url : null,
                            }
                            break;
                        case 'custom':
                            _params = {
                                content_id: reqParams.contentId ? reqParams.contentId : null,
                                custom_transcode_status: data.Item.status ? data.Item.status : null,
                                custom_transcode_url: data.Item.playback_url ? data.Item.playback_url : null,
                            }
                    }


                    // mongDb name
                    _params.mongoDbName = tenantConfig[tenantId].config.mongodb;

                    videoObj.findOneAndUpdate(_params).then((rs) => {
                        console.log('VIDEO UPDATE COMPLETE!!!!');
                    });

                } else {
                    setTimeout(() => {
                        video_utility.getDynamodbStatus(reqParams);
                    }, 2000);
                }
                // resolve(data)
            },
            (err) => {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err.stack, null, 2));
                // reject(err)
            }
        );
    })
};


// To insert into DynamoDB
exports.updateDynamodb = (reqParams) => {
    console.log(" in updatedunmodb in video utility >> " + JSON.stringify(reqParams))
    return new Promise((resolve, reject) => {
        var dynamodb = new AWS.DynamoDB();
        var date = new Date()
        var isoDate= date.toISOString();
        var params = {
            Item: {
                'assetid': { S: reqParams.contentId },
                'job_id_mc': { S: reqParams.jobId },
                'playback_url': { S: reqParams.playback_url },
                'status': { S: 'PROGRESSING' },
                'LastModified': { S:isoDate },
                'custom_resolution' : {SS: reqParams.custom_resolution} 
            },
            TableName: reqParams.tableName
        };
        dynamodb.putItem(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else {
                console.log("updateDynamodbPromise succeeded:." + JSON.stringify(data));
                resolve()
            }
        });
    })
};


//////// /////// ################ %% SNS TRIGGERERING %% ################    ///////////

// Video transcode_asset
exports.transcode_asset = reqParams => new Promise((resolve, reject) => {

    var sns = new AWS.SNS();

    var profiles = reqParams.profiles;
    var tenantId = reqParams.tenantId;
    var clientId = tenantConfig[tenantId].config.clientId
    var TopicArn;
    var date = new Date()
    var isoDate = date.toISOString();

    profiles.forEach((element, i) => {

        switch (profiles[i]) {
            case 'basic':
                TopicArn = awsConfig.SNS_CONFIG.TopicArn.basic;
                break;
            case 'standard':
                TopicArn = awsConfig.SNS_CONFIG.TopicArn.standard;
                break;
            case 'premium':
                TopicArn = awsConfig.SNS_CONFIG.TopicArn.premium;
        }

      var sns_params = {
            Message: JSON.stringify({ clientId: clientId, content_path: reqParams.content_path,LastModified:isoDate}),
            TopicArn: TopicArn,
           };
         console.log("sns params message >> " + sns_params.Message)

        sns.publish(sns_params, function (err, data) {
            if (err) {
                console.log("transcode_asset Publish ERROR >", err, err.stack); // an error occurred
                reject(err)
            } else {
                console.log("transcode_asset Publish SUCCESS for > ");
            }
        });

        if (i == profiles.length - 1) {
            resolve("All SNS Published Successfully!")
        }

    });
})