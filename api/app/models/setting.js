const mongoose = require('mongoose');
const moment = require('moment');

const ConfigurationSchema = mongoose.Schema({
    name: { type: String, required: true },
    config: { type: Object, require: true },
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

ConfigurationSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('Config', ConfigurationSchema, 'Configuration');