const express = require('express');
const app = express();
const router = express.Router();
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
const chalk = require('chalk');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const dotenv = require('dotenv').config();
//console.log(dotenv.parsed)

const mongoDb = require('./config/mongoDb');

// connect to mongoDB
mongoDb.getMongoDbs().then((dbs) => {
    for (let db of dbs) {
        mongoDb.connectMongoDb(db.name)
    }
})

global.ENV = process.env.NODE_ENV || 'development';


// req for middleware
var check_auth = require('./api/middleware/check-auth');



// Routes Which should Handle Request
const video = require('./api/routes/video');
const dashboard = require('./api/routes/dashboard');
const config = require('./api/routes/config');
const profile = require('./api/routes/profile');
const settings = require('./api/routes/settings');
const testimdb = require('./api/routes/testimdb');


/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3001);


app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    limit: '26000mb',
    extended: true,
}));
app.use(bodyParser.json({ limit: '26000mb' }));

app.use(logger('dev'));


app.use('/api', router);
app.use('/config', config);

app.use('/api/video', check_auth, video);
app.use('/api/dashboard', check_auth, dashboard);
app.use('/api/profile', check_auth, profile);
app.use('/api/settings', check_auth, settings);
app.use('/api/testimdb', check_auth, testimdb);
app.use(cookieParser());


// helmet for security purpose
app.use(helmet());

// CORS - To hanlde cross origin requests
app.use(cors());

//compress all responses to Make App Faster
app.use(compression());

mongoose.set('debug', true) // for dev only


/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
    console.log(`%s App is running at http://${process.env.HOST}:%d in %s mode`, chalk.green('✓'), app.get('port'), app.get('env'));
    console.log('  Press CTRL-C to stop\n');
});


module.exports = app;